package ai.maum.stats.reader.redis.message

import kotlinx.serialization.Serializable

@Serializable
data class MessageStatistics(
        var collection: String,
        var payload: String
)