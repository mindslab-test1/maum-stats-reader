package ai.maum.stats.reader.redis.worker

import ai.maum.stats.reader.redis.message.MessageStatistics
import kotlinx.coroutines.delay
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.bson.Document
import org.slf4j.LoggerFactory
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.redis.connection.stream.Consumer
import org.springframework.data.redis.connection.stream.ReadOffset
import org.springframework.data.redis.connection.stream.StreamOffset
import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.stereotype.Service

@Service
class WorkerStatistics(
    var redisTemplate: StringRedisTemplate,
    val mongoTemplate: MongoTemplate
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    private val topic = "ai.maum.stats.kafka.topic.statistics"

    suspend fun run() {
        val consumer = Consumer.from("ai.maum.stats.reader", "reader")
        While@ while (true) {
            try {
                logger.info("[$topic] begin iteration")

                val sop = redisTemplate.opsForStream<String, String>()
                val objectRecords = sop.read(
                    consumer,
                    StreamOffset.create(topic, ReadOffset.lastConsumed())
                ) ?: continue@While

                logger.info("[$topic] retrieved ${objectRecords.size} records from stream")

                val result = mutableMapOf<String, MutableList<String>>()
                For@ for (each in objectRecords) {
                    safeTransaction {
                        val record = Json.decodeFromString<MessageStatistics>(each.value["payload"] ?: return@safeTransaction)
                        val collection = record.collection
                        val payload = record.payload

                        result[collection]
                            ?.apply { add(payload) }
                            ?: result.put(collection, mutableListOf(payload))
                    }
                }

                for (collection in result.keys) {
                    for (item in result[collection]!!) {
                        safeTransaction {
                            logger.info("[$topic] collection:$collection")
                            logger.info("[$topic] payload:$item")
                            saveJsonString(collection, item)
                        }
                    }
                }

                logger.info("[$topic] saved successfully")
            } catch (e: Exception) {
                logger.error("[$topic] ${e.message}")
                logger.debug("[$topic] ${e.stackTraceToString()}")
            }

            // Don't consume CPU too much
            delay(800)
        }
    }

    fun saveJsonString(collection: String, payload: String) {
        val doc = Document.parse(payload)
        mongoTemplate.insert(doc, collection)
    }

    fun safeTransaction(transaction: () -> Unit) {
        try {
            transaction()
        } catch (e: Exception) {
            logger.error("[$topic] ${e.message}")
            logger.debug("[$topic] ${e.stackTraceToString()}")
        }
    }
}