package ai.maum.stats.reader.redis.worker

import ai.maum.stats.reader.jpa.ApiUsage
import ai.maum.stats.reader.jpa.ApiUsageRepository
import ai.maum.stats.reader.redis.message.MessageApiUsage
import kotlinx.coroutines.delay
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.slf4j.LoggerFactory
import org.springframework.data.redis.connection.stream.Consumer
import org.springframework.data.redis.connection.stream.ReadOffset
import org.springframework.data.redis.connection.stream.StreamOffset
import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.stereotype.Service
import java.time.Instant

@Service
class WorkerApiUsage(
        var redisTemplate: StringRedisTemplate,
        var apiUsageRepository: ApiUsageRepository
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    private val topic = "ai.maum.stats.kafka.topic.api-usage"

    suspend fun run() {
        val consumer = Consumer.from("ai.maum.stats.reader", "reader")
        While@ while (true) {
            try {
                logger.info("[$topic] begin iteration")

                val sop = redisTemplate.opsForStream<String, String>()
                val objectRecords = sop.read(
                        consumer,
                        StreamOffset.create(topic, ReadOffset.lastConsumed())
                ) ?: continue@While

                logger.info("[$topic] retrieved ${objectRecords.size} records from stream")

                val result = mutableListOf<ApiUsage>()
                For@ for (each in objectRecords) {
                    val record = Json.decodeFromString<MessageApiUsage>(each.value["payload"] ?: continue@For)

                    val usage = ApiUsage(
                            apiId = record.apiId,
                            elapsedTime = record.elapsedTime,
                            engine = record.engine,
                            service = record.service,
                            requestAosPackage = record.requestAosPackage,
                            requestIosUrlScheme = record.requestIosUrlScheme,
                            requestApplicationName = record.requestApplicationName,
                            requestHost = record.requestHost,
                            requestMethod = record.requestMethod,
                            requestPayloadSize = record.requestPayloadSize,
                            requestTime = record.requestTime.let { Instant.ofEpochMilli(record.requestTime) },
                            requestContentType = record.requestContentType,
                            resource = record.pathAbsolute,
                            responsePayloadSize = record.responsePayloadSize,
                            responseStatusCode = record.responseStatusCode,
                            responseTime = record.responseTime.let { Instant.ofEpochMilli(record.responseTime) },
                            responseContentType = record.responseContentType,
                            success = record.success,
                            uuid = record.uuid
                    )

                    result.add(usage)
                }

                apiUsageRepository.saveAll(result)

                logger.info("[$topic] saved successfully")
            } catch (e: Exception) {
                logger.error("[$topic] ${e.message}")
                logger.debug("[$topic] ${e.stackTraceToString()}")
            }

            // Don't consume CPU too much
            delay(800)
        }
    }
}