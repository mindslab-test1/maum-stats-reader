package ai.maum.stats.reader.redis

import ai.maum.stats.reader.redis.worker.WorkerApiUsage
import ai.maum.stats.reader.redis.worker.WorkerStatistics
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component

@Profile("!test")
@Component
class RedisRunner(
        var workerApiUsage: WorkerApiUsage,
        var workerStatistics: WorkerStatistics
) : ApplicationRunner {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun run(args: ApplicationArguments) {
        GlobalScope.launch {
            launch {
                workerApiUsage.run()
            }
            launch {
                workerStatistics.run()
            }
        }
    }
}
