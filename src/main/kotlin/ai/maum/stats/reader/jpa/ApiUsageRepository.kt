package ai.maum.stats.reader.jpa

import org.springframework.data.jpa.repository.JpaRepository

interface ApiUsageRepository : JpaRepository<ApiUsage, Long> {
}