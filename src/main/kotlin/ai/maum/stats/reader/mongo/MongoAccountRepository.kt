package ai.maum.stats.reader.mongo

import org.springframework.data.mongodb.repository.MongoRepository

interface MongoAccountRepository : MongoRepository<Account, Long> {
}