package ai.maum.stats.reader.mongo

import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.data.mongodb.core.MongoTemplate

//@Component
class MongoRunner(
        var mongoTemplate: MongoTemplate
) : ApplicationRunner {
    override fun run(args: ApplicationArguments) {
    }

    fun noRun() {
        var account = Account()
        account.email = "b@b.c"
        account.name = "bob"

        account = mongoTemplate.insert(account)
    }
}
