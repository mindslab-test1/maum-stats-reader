package ai.maum.stats.reader.mongo

import ai.maum.stats.reader.jpa.AllOpen
import org.springframework.data.mongodb.core.mapping.Document
import javax.persistence.Id

@AllOpen
@Document(collection = "accounts")
class Account {
    @Id
    var id: String? = null

    var name: String? = null
    var email: String? = null
}